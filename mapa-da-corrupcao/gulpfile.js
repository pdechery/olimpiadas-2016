// gulpfile

var gulp 	= require('gulp'),
	mincss 	= require('gulp-clean-css'),
	less 	= require('gulp-less');

gulp.task('css',function(){
	return gulp.src('style/less/*.less')
		.pipe(less())
		.pipe(mincss({debug: true}))
		.pipe(gulp.dest('style'));
});

gulp.task('watch', function(){
	gulp.watch('style/less/*.less',['css']);
});

gulp.task('default', ['watch']);