/*

	Visualização com picos de tweets e manchetes

 */

(function mencoesPicos() {

	// stage

	var picos = d3.select('#mencpic').append('svg'),
		width = 1000,
		height = 600;

	picos.attr('width', width).attr('height', height);

	var margin = {top: 0, right: 0, bottom: 40, left: 40},
		width = width - margin.left - margin.right,
		height = height - margin.top - margin.bottom;
		
	var g = picos.append("g")
		.attr('id', 'bar_chart')
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	// transition

	var t = d3.transition()
		.duration(1000)
		.ease(d3.easeLinear);

	// janela chamada

	d3.select('#mencpic').append('div')
		.attr('class','hidden chamada');

	// d3 time

	var parseDate = d3.timeParse('%m/%d/%Y'),
		formatDate = d3.timeFormat('%d/%m/%Y');

	// import

	d3.csv('./data/jana_picos_tweets_ano.csv', function(d) {

		return {
			data: d.data,
			tweets: +d.tweets,
			chamada: d.chamada,
			link: d.link,
			tema: d.tema
		}

	}, function(err, data) {

		if (err) throw err;
		
		Array.prototype.unique = function() {
			var un = [];
			for (var i = this.length - 1; i >= 0; i--) {
				if (un.indexOf(this[i]) == -1) un.push(this[i]);
			}
			return un;
		};

		/**
		 * groupByData
		 *
		 * @description agrupa dados pela data, criando objetos separados para cada tema
		 */
		
		var groupByData = d3.nest()
			.key(function(d) {
				return d.data
			})
			.entries(data);

		/**
		 * getYears
		 *
		 * @description extrai anos do dataset para popular o filtro
		 * @return {array} array com strings dos anos
		 */

		var getYears = groupByData.map(function(el,i){

			return d3.timeFormat('%Y')(parseDate(el.key));

		}).unique().sort();

		/**
		 * @name getDataAno
		 *
		 * @description filtra dataset (data) por ano usando moment.js
		 * parseia a data como string vinda do dado para usar com o o moment.js
		 *
		 * @param {number} [ano]
		 * @return {array} Array de objetos do dataset
		 */

		var getDataAno = function(ano = '2014') {
			
			var ano = ano;
			
			return groupByData.filter(function(el, i, arr){
				
				return moment('01/01/'+ano).isSame(parseDate(el.key), 'year');
			
			});

		};

		var dias = getDataAno().map(function(d,i){
			return formatDate(parseDate(d.key));
		});

		var valores = getDataAno().map(function(d,i){
			return d.values[0].tweets;
		}).sort(function(a,b){
			return a - b;
		});

		/*
		
			Escalas

		 */
		
		var x = d3.scaleBand()
			.domain(dias)
			.paddingInner([.3])
			.rangeRound([0, width]);

		var y = d3.scaleLinear()
			.domain([0, d3.max(valores)])
			.rangeRound([height,0]);

		/*
		
			Eixos

		 */ 

		function customEixoY(g) {
			g.call(eixoY);
			g.select('.domain').remove();
			g.selectAll('.tick line').attr('stroke','#b9c2d8');
		};
		
		var eixoY = d3.axisLeft(y)
			.tickFormat(d3.format(".2s"));

		/*
			
			Filtro

		 */
		
		var filtroAno = d3.select('#mencoes_picos header').insert('form','h3')
			.append('select')
			.attr('id','filtro_ano_tweets')
			.selectAll('option')
			.data(getYears)
			.enter()
			.append('option')
			.attr('value', function(d){
				return +d;
			})	
			.text(function(d){
				return d;
			});

		makeTweetBarChart(getDataAno());

		/*
		
			Update

		 */
		
		document.querySelector('#filtro_ano_tweets').addEventListener('change', function(evt){

			var ano = evt.target.value;

			// extrair novos dados

			var upData = getDataAno(ano);

			var newDias = upData.map(function(el,i){
				return formatDate(parseDate(el.key));
			});

			var newValores = upData.map(function(d,i){
				return d.values[0].tweets;
			}).sort(function(a,b){
				return a - b;
			});

			// atualizar escalas e eixos

			x.domain(newDias);

			y.domain([0, d3.max(newValores)]);

			eixoY.scale(y);

			// plot

			makeTweetBarChart(upData);

		});

		// eventos

		function evts(rect){

			rect.on('mouseenter', function(d,i){

				d3.select(this).classed('active', true);

				var data = d;
				
				d3.select('.chamada').classed('hidden', false)
					.style('top', "50%")
					.style('left', "50%")
					.style('margin-top', "-150px")
					.style('margin-left', "-200px")
					.html(function(){
						var html = "<h4>"+data.values[0].chamada+"</h4>";
						html += "<p>Alcance: "+data.values[0].tweets+" tweets</p>";
						html += "<p>Data: "+formatDate(parseDate(data.key))+"</p>";
						return html;
					});

			})

			rect.on('mouseleave', function(d,i){

				d3.select(this).classed('active', false);

				d3.select('.chamada').classed('hidden', true);

			});

		}

		/*
		
			Plot
			
		 */

		function makeTweetBarChart(_data_) {

			var bars = g.selectAll('.bars').data(_data_);

			bars.enter()
				.append('rect')
				.attr('class','bars')
				.merge(bars)
				.attr('class','bars update')
				.attr('x', function(d){
					return x(formatDate(parseDate(d.key)));
				})
				.attr('width', x.bandwidth())
				.attr('y', function(d){
					var v = d.values[0].tweets;
					return y(v);
				})
				.attr('height', function(d){
					var v = d.values[0].tweets;
					return height - y(v);
				})
				.call(evts);

			bars.exit().attr('class', 'delete').remove();

			// eixos

			if (!document.querySelector('#eixo_y')) {

				picos.append('g')
					.attr('id','eixo_y')
					.attr("transform", "translate("+margin.left+",0)")
					.call(customEixoY);

			} else {

				d3.select('#eixo_y').call(customEixoY);

			}

		};

	}); // d3.csv

})();