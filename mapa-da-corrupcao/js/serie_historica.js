/*
	
	Série Histórica com filtro de tipos de processos

 */

(function serieHistorica(){

	var NomesCrimes = [
		{leiLicit:"Crimes da Lei de Licitações"},
		{crimResp:"Crimes de Responsabilidade"},
		{crimPartAdmGer:"Crimes Praticados por Particular Contra a Administração em Geral"},
		{imprAdm:"Improbidade Administrativa"}
	];

	var serie = d3.select('#srhist').append('svg'),
		width = 1000,
		height = 600;

	serie.attr('width',width).attr('height',height);

	var margin = {top: 30, right: 0, bottom: 40, left: 40},
		width = width - margin.left - margin.right,
		height = height - margin.top - margin.bottom;
		
	var g = serie.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	// transition

	var t = d3.transition().duration(750);

	// tooltip

	var tooltipStack = d3.select('body').append('div')
		.attr('class', 'hidden tooltip-stack');

	d3.json('./data/processos_ano_quantidade.json', function(data){

		// eventos

		function evts(el){
			el.on('click', function(d){
				console.log(d);
			});
			el.on('mousemove', function(d){
				var datum = d,
					content = [];
				tooltipStack.classed('hidden', false)
					.attr('style', 'left:' + (d3.event.pageX + 10) + 'px; top:' + (d3.event.pageY + 10) + 'px;')
					.html(function(){
						
						var cont = '';

						if (datum[0] != 0) { // se não é dado filtrado
							
							cont += '<p>Ano: ' + datum.data.ano + '</p>';
							for (d in datum.data){
								if (datum.data.hasOwnProperty(d)) {
									if (d !== 'ano' && d !== 'total') {
										var val = +datum.data[d];
										cont += '<span class="' + d +'"></span> ' +  val + '<br>';
									}
								}
							}
							cont += '<p>Total: ' + datum.data.total + '</p>';

						} else {
							
							cont += '<strong>' + datum[1] + '</strong> processos<br>';
							cont += 'Ano: ' + datum[2].ano;
						}

						return cont;

					});
			})
			el.on('mouseout', function(d){
				tooltipStack.classed('hidden', true);
			});
		}

		// nomes crimes

		var crimes = ["crimPartAdmGer","leiLicit","crimResp","imprAdm"];

		// anos

		var anos = data.map(function(el){
			return el.ano;
		});

		// stack function

		var crimeStack = d3.stack().keys(crimes)(data);

		// usar última linha do array crimeStack para descobrir o valor máximo produzido pelo d3.stack()

		var maxValor = d3.max(crimeStack[crimeStack.length - 1], function(el){
			return el[1];
		});

		// escalas

		var x = d3.scaleBand()
			.domain(anos)
			.paddingInner([.6])
			.rangeRound([0, width]);

		var y = d3.scaleLinear()
			.rangeRound([height, 0]);

		var colors = d3.scaleOrdinal()
			.range(['#384975','#c23d48','#e1d9b3','#fff8d8'])
			.domain(crimes);

		// eixos 

		function customEixoX(g) {
			g.call(eixoX);
			g.select('.domain').remove();
			g.selectAll('.tick line').remove();
		};

		function customEixoY(g) {
			g.call(eixoY);
			g.select('.domain').remove();
			g.selectAll('.tick line').attr('stroke','#b9c2d8');
		};

		var eixoY = d3.axisLeft(y)
			.tickSize(-width) // largura negativa
			.tickPadding(12)
			.tickFormat(d3.format('.2s'));

		var eixoX = d3.axisBottom(x)
			.tickSizeInner(15);

		// plotando

		// select

		var select = d3.select('#serie_historica header')
			.insert('form', 'h3')
			.attr('id','processos_mp')
			.append('select');

		select
			.selectAll('option').data(NomesCrimes)
			.enter().append('option')
			.text(function(d){
				return d[Object.keys(d)[0]];
			})
			.attr('value', function(d){
				return Object.keys(d)[0];
			});

		var all = select.insert('option')
			.attr('value','all')
			.attr('selected', true)
			.text('Todos os Processos');

		all.lower();

		// eixo x

		g.append("g")
			.attr("class", "x axis")
			.attr("transform", "translate(0," + height + ")")
			.call(customEixoX);

		// legendas

		var legendasContainer = d3.select('#srhist').append('div')
			.attr('id','legendas-container')
			.append('svg')
			.attr('width', 1000)
			.attr('height', 200);

		// col1

		var legendasCol1 = legendasContainer.append('g')
			.attr('class', 'legendas')
			.attr("transform", "translate(0,0)")
			.selectAll('g').data(NomesCrimes.slice(0,2))
			.enter().append('g')
			.attr("transform", function (d, i) {
				return "translate(0," + (i * 40) + ")";
			});

		// col2

		var legendasCol2 = legendasContainer.append('g')
			.attr('class', 'legendas')
			.attr("transform", "translate(400,0)")
			.selectAll('g').data(NomesCrimes.slice(2))
			.enter().append('g')
			.attr("transform", function (d, i) {
				return "translate(0," + (i * 40) + ")";
			});

		function createLegenda(legd) {

			legd.append('rect')
				.attr('class', function(d){
					return 'box ' + Object.keys(d)[0];
				})
				.attr("width", 25)
				.attr("height", 25)
				.attr("x", 0)
				.attr("y", 0);

			legd.append('text')
				.attr('class', 'label')
				.attr("x", 35)
				.attr("y", 20)
				.text(function(d){
					return d[Object.keys(d)[0]];
				});

		};

		createLegenda(legendasCol1);
		createLegenda(legendasCol2);


		// criar vis principal

		var layers;

		var createMainVis = function() {

			// atualiza escala

			y.domain([0, maxValor]);

			// eixo y

			d3.select('.y.axis').remove();

			g.append("g")
				.attr("class", "y axis")
				.call(customEixoY);

			// vis

			var layersArea = g.append('g').attr('class', 'series');

			layers = layersArea.selectAll('g')
					.data(crimeStack)
					.enter()
					.append('g')
					.attr('class', function(d){
						return 'enter '+d.key;
					});

			layers.selectAll('rect')
					.data(function(d) {
						return d;
					})
					.enter()
					.append('rect')
					.attr('x', function(d){
						return x(d.data.ano);
					})
					.attr('width', function(){
						return x.bandwidth();
					})
					.attr('y', function(d){
						return y(d[1]);
					})
					.call(evts)
					.transition(t)
					.attr('height', function(d){
						return y(d[0]) - y(d[1]);
					});

		};

		createMainVis();

		// update/filtro

		document.querySelector('#processos_mp select').onchange = function(ev){
			
			var val = ev.target.value;

			if (val == 'all') {

				// reconstruir grafico, melhor apagar tudo e recriar com enter()

				layers.remove();

				createMainVis();

			
			} else {

				// novo dataset imitando resultado do d3.stack
				// com todos os rects começando do zero (y0) e com 
				// altura correspondente ao valor apresentado no dado para o ano

				var newStuff = data.map(function(el,i){ 
					var arr = [ 0, el[val], {key: val, ano: 2012+i}];
					return arr;
				});

				// valor máximo presente no novo dataset para atualizar domain y

				var max = d3.max(newStuff, function(el){ return el[1] });

				y.domain([0,max]);

				// eixo y	

				d3.select('.y.axis').selectAll('tick').remove();

				g.select('.y.axis').call(customEixoY);

				// plot

				var newPlot = layers.data([newStuff]);

				newPlot.exit().remove();

				newPlot.attr('class', 'update '+val);

				newPlot.selectAll('rect')
					.data(function(d){
						return d;
					})
					.attr('x', function(d,i){
						return x(d[2].ano);
					})
					.attr('width', function(){
						return x.bandwidth();
					})
					.attr('y', function(d){
						return y(d[1]);
					})
					.transition(t)
					.attr('height', function(d){
						return y(d[0]) - y(d[1]);
					});

			}

		};

	});

})();