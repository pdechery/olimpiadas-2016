 /*
	
	Processos e Tweets por UF

 */

// queue

d3.queue()
	.defer(d3.json, './data/brasil_ufs.json') // mapa brasil topoJSON
	.defer(d3.json, './data/processos_agregado_uf_2016.json')
	.defer(d3.json, './data/mencoes_uf_ano_2016.json')
	.await(procTweetsUf);

function procTweetsUf(error, uf, processos, tweets){

	if (error) throw error;

	// obj para o filtro

	var filtros = [
		{
			nome:  'Corrupção',
			valor: 'corrup'
		},
		{
			nome:  'Lava-Jato',
			valor: 'lavajato'
		}
	];

	/**
	 * Obter centros da UFs (centróides) a partir das coordenadas do topojson
	 *
	 * @param {array} [coords] coordenadas do topojson 
	 * @param {string} [af] UF do estado
	 * @return {obj} Objeto com coordenadas e string da UF
	 */

	var __getUFCenter = function(coords, uf){

		var resArray = coords[0].length == 1 ? coords[0][0] : coords[0];

		var xLon = d3.extent(resArray, function(val,i){
			return val[0];
		});

		var xLat = d3.extent(resArray, function(val,i){
			return val[1];
		});

		var centroX = xLon[0] + ( (xLon[1] - xLon[0]) / 2 );
		var centroY = xLat[0] + ( (xLat[1] - xLat[0]) / 2 );

		return {
			uf: uf || '',
			coords: [centroX, centroY]
		};

	};

	/*
	
		Stage

	 */

	var w = 1000,
		h = 800;

	var procTweetUF = d3.select('#proctweet')
		.append('svg')
		.attr('width', w)
		.attr('height', h);

	/*
		
		Projection

	 */

	var mercatorProjection = d3.geoMercator()
		.scale( 1000 )
		.translate( [w/2.2, h/2.2] );

	/**
	 * Obter latlong das UFs do Brasil a partir do topojson
	 *
	 * @return {array} Array de objetos
	 * @see https://github.com/topojson/topojson
	 */

	var BRlatLong = topojson.feature(uf, uf.objects.uf).features;

	/**
	 * Obter array com centróides das UFs (coordenadas)
	 *
	 * @return {array} Array de arrays com centróides de cada UF e string com nome das mesmas
	 */

	var centrosUF = BRlatLong.map(function(val, i){
		return __getUFCenter(val.geometry.coordinates, val.id);
	});

	// centralizar mapa

	mercatorProjection.center( __getUFCenter(BRlatLong[12].geometry.coordinates).coords);  // 'BRlatLong[12]' = DF

	/*
	
		Debulhar Dados

	 */

	var processosUF = processos.map(function(val){
		return {
			'p': +val.quant,
			'uf': val.UF
		};
	});

	var tweetsUF = [];

	tweets.forEach(function(val, i){

		var tweetUf = val.UF;

		var dados = {
			'corrup': +val.corrup,
			'lavajato': +val.lavajato,
			'processos': 0,
			'uf': val.UF
		};

		processosUF.forEach(function(pr){

			if (pr.uf == tweetUf) dados.processos = pr.p;

		})

		tweetsUF.push(dados);

	});
	
	/**
	 * Pegar corrdenadas a partir de string de UF
	 *
	 * @param {string} [uf] sigla uf
	 * @return {array} array com lat e long
	 */

	function latLong(uf){

		var c = centrosUF.filter(function(el,i){
			return el.uf == uf;
		});

		return c[0].coords;
	}

	/*
		Escala Cor

	 */

	var color = d3.scaleLog()
		.base(.5)
		.range(["#2F406A","#1F2C54"])
		.domain(d3.extent(processosUF, function(el){ return el.p; }));

	/*
		
		Escala tamanho imagens

	 */
	
	var tweetsDomain = [];
	
	tweetsUF.forEach(function(dado,i){
		if (tweetsDomain.indexOf(dado.corrup) === -1) {
			tweetsDomain.push(dado.corrup);
		}
		return tweetsDomain;
	});
	
	var makeMegaFone = d3.scaleLinear()
		.range([20,50])
		.domain(d3.extent(tweetsDomain));


	/*
	
		Plot

	 */
	
	// filtro

	var filtroTweets = d3.select('#processos_tweets_ufs')
		.insert('form','.center_1260')
		.attr('id','filtro_tweets');
		
	filtroTweets.append('select')
		.selectAll('option').data(filtros)
		.enter().append('option')
		.attr('value', function(d){
			return d.valor;
		})	
		.text(function(d){
			return d.nome;
		});

	// tabela legendas

	var colunas = ['corrup','lavajato','processos','uf'];

	var table = d3.select('#processos_tweets_ufs').append('table').attr('id','info_table_tweets'),
		thead = table.append('thead');

	table.on('mouseout', function(el,i) {
		d3.selectAll('.circle_info').classed('hidden', true);
	})

	thead.append('tr').selectAll('th')
		.data(colunas)
		.enter()
		.append('th')
		.text(function(el,i){
			return el;
		})

	var rows = table.selectAll('tr')
		.data(tweetsUF)
		.enter()
		.append('tr')
		.attr('data-uf', function(el,i){
			return el.uf;
		})
		.on('mouseover', function(el,i){

			d3.selectAll('.bola circle')
				.transition(t)
				.attr('r', function(d,i){
					return makeMegaFone(d[selec]);
				});

			d3.select(`.bola.${el.uf}`).select('circle')
				.transition(t)
				.attr('r', function(d,i){
					return makeMegaFone(d[selec]) + 10;
				});

			d3.select(`.circle_info.${el.uf}`).classed('hidden', false);

		});

	rows.selectAll('td')
		.data(function(data){
			return colunas.map(function(el,i){
				return {'coluna': el, 'valor': data[el]};
			})
		})
		.enter()
		.append('td')
		.text(function(el,i){
			return el.valor;
		});

	// mapa
	
	var path = d3.geoPath().projection(mercatorProjection); // o dado que vai para o D3

	procTweetUF.append('g')
		.attr('id','map')
		.selectAll('path')
		.data(BRlatLong)
		.enter()
		.append('path')
		.attr('d', path)
		.attr('fill', function(d,i){
			if (processosUF[i]) {
				return color(processosUF[i].p);
			} else {
				return '#374977'; // valor UF sem dado. editar no dataset para vir 0?
			}
		})
		.on('mouseover', function(el,i){
			d3.selectAll('.circle_info').classed('hidden', true);
		});

	// wrapper tweets

	var wrapImg = procTweetUF.append('g').attr('id','tweet');

	var wrapTxt = procTweetUF.append('g').attr('id','info');

	__plotMegaFones(tweetsUF);

	// transition

	var t = d3.transition().duration(750);

	/*
	
		Update

	 */
	
	 // filtro padrão

	var selec = 'corrup';
	
	document.querySelector('#filtro_tweets').onchange = function(ev){

		selec  = ev.target.value;

		__plotMegaFones(tweetsUF, selec);

	}

	/*
	
		Plot Function

	 */

	function __plotMegaFones(data, opt = 'corrup') {

		var circlesTweetsData = wrapImg.selectAll('.bola').data(data);

	 	var enterG = circlesTweetsData.enter()
	 		.append('g')
	 		.attr('class', function(d,i){
	 			return `bola ${d.uf}`;
	 		});

	 	enterG.append('circle')
	 		.attr('cx', function(d,i){
	 			var ll = latLong(d.uf);
				return mercatorProjection(ll)[0];
	 		})
	 		.attr('cy', function(d,i){
	 			var ll = latLong(d.uf);
				return mercatorProjection(ll)[1];
	 		})
	 		.attr('r', function(d,i){
	 			return makeMegaFone(d[opt]);
	 		});

	 	enterG.append('image')
			.attr('xlink:href','img/icone_megafone_pixelado.svg')
			.attr('x', function(d,i){
				var ll = latLong(d.uf);
				return mercatorProjection(ll)[0];
			})
			.attr('y', function(d,i){
				var ll = latLong(d.uf);
				return mercatorProjection(ll)[1];
			})
			.attr('transform', function(d,i){
				var s = makeMegaFone(d[opt]),
					d = (s/2) * -1;
				return `translate(${d},${d})`;
			})
			.attr('width', function(d,i){
				return makeMegaFone(d[opt]);
			})
			.attr('height', function(d,i){
				return makeMegaFone(d[opt]);
			})
			.call(__MegaFoneEvents);

		circlesTweetsData.selectAll('circle')
			.attr('cx', function(d,i){
	 			var ll = latLong(d.uf);
				return mercatorProjection(ll)[0];
	 		})
	 		.attr('cy', function(d,i){
	 			var ll = latLong(d.uf);
				return mercatorProjection(ll)[1];
	 		})
	 		.attr('r', function(d,i){
	 			return makeMegaFone(d[opt]);
	 		});

	 	circlesTweetsData.selectAll('image')
			.attr('transform', function(d,i){
				var s = makeMegaFone(d[opt]),
					d = (s/2) * -1;
				return `translate(${d},${d})`;
			})
			.attr('width', function(d,i){
				return makeMegaFone(d[opt]);
			})
			.attr('height', function(d,i){
				return makeMegaFone(d[opt]);
			})


	 	// plot circle info
	 	
	 	var circlesInfoData = wrapTxt.selectAll('.circle_info').data(data);

	 	var enterG2 = circlesInfoData.enter()
	 		.append('g')
	 		.attr('class', function(el,i){
	 			return `circle_info ${el.uf} hidden`;
	 		})
	 		.attr('transform', function(el,i){
				var r = makeMegaFone(el[opt]) + 6;
				return `translate(${r}, ${r * -1})`; // x y
			});

		enterG2.append('circle')
			.attr('r', '25')
			.attr('cx', function(d,i){
				var ll = latLong(d.uf);
				return mercatorProjection(ll)[0];
			})
			.attr('cy', function(d,i){
				var ll = latLong(d.uf);
				return mercatorProjection(ll)[1];
			});

	 	enterG2.append('text')
	 		.attr('x', function(d,i){
	 			var ll = latLong(d.uf);
				return mercatorProjection(ll)[0];
	 		})
	 		.attr('y', function(d,i){
	 			var ll = latLong(d.uf);
				return mercatorProjection(ll)[1];
	 		})
	 		.attr('dx', -14)
			.attr('dy', 5)
	 		.text(function(d,i) {
	 			return d[opt];
	 		});

		circlesInfoData.selectAll('text')
			.text(function(d,i) {
	 			return d[opt];
	 		});

	};

	function __MegaFoneEvents(el){

		el.on('mouseover', function(d,i){
			
			d3.selectAll('.bola')
				.transition(t)
				.attr('r', function(d,i){
					return makeMegaFone(d[selec]);
				});

			d3.select(this.previousSibling)
				.transition(t)
				.attr('r', function(d,i){
					return makeMegaFone(d[selec]) + 10;
				});

			d3.select(`.circle_info.${d.uf}`).classed('hidden', false);

		});

		el.on('mouseout', function(d,i){

			d3.select(this.previousSibling)
				.transition(t)
				.attr('r', function(d,i){
					return makeMegaFone(d[selec]);
				});

		});

	};

};