/*
	Mapa da Corrupção
	Agosto de 2017
	Pierre Dechery

 */

// var minheader = new Waypoint({
// 	element: document.querySelector('#mapa_processos_acumulado'),
// 	handler: function(dir) {
// 		console.log(d3.select('#mini-header'));
// 		d3.select('#mini-header').classed('show', function(){
// 			return dir == 'down' ? true : false;
// 		})
// 	},
// 	offset: -90
// })

var pig = new Waypoint({
	element: document.querySelector('#mapa_processos_acumulado'),
	handler: function(dir) {
		d3.select('#botaopig').classed('show', function(){
		 	return dir == 'down' ? true : false;
		})
	}
})

document.querySelector('#botaopig').addEventListener('click', function(){

	d3.select('#info').classed('show', true);

});

document.querySelector('.overlay .close').addEventListener('click', function(){

	d3.select('#info').classed('show', false);

});