 /*
	
	Mapa Ministério Público Processos UFs

 */

// queue

d3.queue()
	.defer(d3.json, './data/brasil_ufs.json') // mapa brasil topoJSON
	.defer(d3.json, './data/ano_uf_processo_quantidade.json') 
	.await(mapaProcessosUF);

// mapeando nomes de processos com ID

var Processos = [
	{nome: 'Crimes Praticados Contra a Administração em Geral', slug: 'AdmG', color: '#f5b0ad'},
	{nome: 'Crimes da Lei de Licitações', slug: 'Licit', color: '#e2dab3'},
	{nome: 'Crimes de Responsabilidade', slug: 'Resp', color: '#f05461'},
	{nome: 'Improbidade Administrativa', slug: 'ImpAdm', color: '#fff'}
];

var slugProcessos = {
	'Crimes da Lei de Licitações':'Licit',
	'Crimes de Responsabilidade':'Resp',
	'Crimes Praticados Contra a Administração em Geral':'AdmG',
	'Improbidade Administrativa':'ImpAdm'
};

var coresProcessos = {
	'Crimes da Lei de Licitações':'#e2dab3',
	'Crimes de Responsabilidade':'#f05461',
	'Crimes Praticados Contra a Administração em Geral':'#f5b0ad',
	'Improbidade Administrativa':'#fff'
};

// anos

var anos = ['2012', '2013', '2014', '2015', '2016', '2017'];

function mapaProcessosUF(error, uf, mp) {

	// stage

	var w = 1000,
		h = 800;

	// elemento svg

	var mpUf = d3.select('#procuf')
		.append('svg')
		.attr('width', w)
		.attr('height', h);

	// projection

	var mercatorProjection = d3.geoMercator()
		.scale( 1000 )
		.translate( [w/2.2, h/2.2] );

	// path

	var path = d3.geoPath().projection(mercatorProjection);

	// transition

	var t = d3.transition().duration(750);

	// escolhas default dos filtros

	var selec = 'AdmG',
		ano = '2012';

	/**
	 * Obter centro da UF a partir das coordenadas do topojson
	 *
	 * @return {obj} Objeto com coordenadas e string da UF
	 */

	var __getUFCenter = function(coords, uf){

		var resArray = coords[0].length == 1 ? coords[0][0] : coords[0];

		var xLon = d3.extent(resArray, function(val,i){
			return val[0];
		});

		var xLat = d3.extent(resArray, function(val,i){
			return val[1];
		});

		var centroX = xLon[0] + ( (xLon[1] - xLon[0]) / 2 );
		var centroY = xLat[0] + ( (xLat[1] - xLat[0]) / 2 );

		return {
			uf: uf || '',
			coords: [centroX, centroY]
		};

	};

	if (error) throw error;

	/**
	 * Obter latlong das UFs do Brasil a partir do topojson
	 *
	 * @return {array} Array de objetos
	 * @see https://github.com/topojson/topojson
	 */
	
	var BRlatLong = topojson.feature(uf, uf.objects.uf).features;

	/**
	 * Obter array com centros das UFs (coordenadas)
	 *
	 * @return {array} Array de objetos com coordenadas e UF
	 */

	var centrosUF = BRlatLong.map(function(val,i){
		return __getUFCenter(val.geometry.coordinates, val.id);
	});

	/**
	 * Dataset para alimentar a visualização
	 *
	 * @return {array} Array de objetos com número de processos (num), UF (string) e coordenadas (array)
	 *
				
		Object {
			key: 'AF',
			value: {
				coords: [0,1],
				processos: [
					{
						pid: {slug Processo},
						processo: {Nome Processo},
						cor: {Cor Processo},
						quant: 2
					},
					{
						pid: {slug Processo},
						processo: {Nome Processo},
						cor: {Cor Processo},
						quant: 3
					},
					{
						pid: {slug Processo},
						processo: {Nome Processo},
						cor: {Cor Processo},
						quant: 45
					}
				],
				uf: 'AF'
			}
		}

	 */

	var dataset = d3.nest()
		.key(function(d){
			return d.ano;
		})
		.key(function(d){
			return d.uf;
		})
		.rollup(function(data){
			
			var dadosProcessos = data.map(function(el,i){

				var processos = {
					processo: el.processo,
					pid: slugProcessos[el.processo],
					cor: coresProcessos[el.processo],
					quant: +el.quantidade,
				};

				return processos;

			});

			var latLong = centrosUF.filter(function(el,i){
				return el.uf == data[0].uf;
			})[0].coords;

			return {
				uf: data[0].uf,
				coords: latLong,
				processos: dadosProcessos
			}

		})
		.entries(mp); // entries / object

	// centralizar mapa (coords MT)

	mercatorProjection.center( __getUFCenter(BRlatLong[12].geometry.coordinates).coords );

	/**
	 * Filtrar dataset por ano
	 *
	 * @return {array} Array pronto para ser usado no D3
	 */

	function getYearDataset(ano){

		return dataset.filter(function(el,i){
			return el.key == ano;
		})[0].values;

	};

	/**
	 * Função para determinar limites (min,max) da quantidade de processos num determinado ano
	 *
	 * @param {string} [pid] slug do processo
	 * @description Usada para cálculo do raio dos circles
	 * @return {array} Array com valor mínimo e máximo encontrado
	 */

	function __findDomain(pid) {

		var pvalues = [];

		var defaultYearDataset = getYearDataset(ano);

		defaultYearDataset.forEach(function(el,i){

			el.value.processos.forEach(function(el,i){

				if (el.pid == pid) {

					var quant = el.quant;

					if (pvalues.indexOf(quant) === -1) {

						pvalues.push(quant);

					}

				}

			});

		});

		return d3.extent(pvalues);

	}

	// escala quadrados

	var rScale = d3.scaleLinear()
		.range([10,160]);

	// helper functions

	var filterProcess = function(data, pid){
		return data.value.processos.filter(function(el,i){
			return el.pid == pid;
		})[0];
	};

	/*
	
		Plot

	 */

	// filtro processos

	var Form = d3.select('#processos_uf header').insert('form','h3')
		.attr('id','filtros');

	var filtroProc = Form.append('select')
		.attr('id','filtro-processos');

	var filtroAnos = Form.append('select')
		.attr('id','filtro-anos');

	filtroProc
		.selectAll('option').data(Processos)
		.enter().append('option')
		.attr('value', function(d){
			return d.slug;
		})	
		.text(function(d){
			return d.nome;
		});

	// filtro anos

	filtroAnos
		.selectAll('option').data(anos)
		.enter().append('option')
		.attr('value', function(d){
			return d;
		})	
		.text(function(d){
			return d;
		});

	// plotar mapa

	mpUf.append('g')
		.attr('class', 'map')
		.selectAll('path')
		.data(BRlatLong)
		.enter()
		.append('path')
		.attr('d', path)
		.style('fill', '#808caa')
		.style('stroke','#b9c2d8');

	// boxes

	var boxesContainer = d3.select('#procuf').append('div').attr('id','boxes');

	__makeBoxes(ano, selec);

	/*
	
		Update Processo e Ano

	 */

	document.querySelector('#filtro-processos').onchange = function(ev){

		selec  = ev.target.value; // atualiza selec (slug processo)

		__makeBoxes(ano, selec);

	}

	document.querySelector('#filtro-anos').onchange = function(ev){

		ano = ev.target.value; // atualiza ano

		__makeBoxes(ano, selec);

	}

	/*
	
	 */
	
	function __makeBoxes(ano = '2012', selec = 'AdmG') {

		rScale.domain(__findDomain(selec));

		var boxes = boxesContainer.selectAll('.box')
			.data(getYearDataset(ano));

		boxes.exit().remove();
		
		boxes.enter()
			.append('div')
			.merge(boxes)
			.attr('class', function(d,i){
				return 'box ' + d.key;
			})
			.style('display', function(d,i){
				var pr = filterProcess(d, selec);
				return pr ? 'block' : 'none';
			})
			.style('left', function(d,i){
				return mercatorProjection(d.value.coords)[0] + 'px';
			})
			.style('top', function(d,i){
				return mercatorProjection(d.value.coords)[1] + 'px';
			})
			.style('width', function(d,i){
				var pr = filterProcess(d, selec);
				return (pr ? rScale(pr.quant) : 10) + 'px';
			})
			.style('height', function(d,i){
				var pr = filterProcess(d, selec);
				return (pr ? rScale(pr.quant) : 10) + 'px';
			})
			.style('transform', function(d,i){
				var pr = filterProcess(d, selec),
					t = pr ? rScale(pr.quant) : 10,
					t = (t/2) * -1 + 'px';
				return `translate(${t},${t})`;
			})
			.style('background-color', function(d,i){
				var pr = filterProcess(d, selec);
				return pr ? pr.cor : '#000';
			})
			.html(function(d,i){
				var pr = filterProcess(d, selec),
					q = pr ? pr.quant : '';
				return `<span class="hidden">${d.key}: ${q} processos</span>`;
			})
			.on('mouseover', function(d,i){

				d3.select(this).style('opacity', 1);

				d3.select(this).style('z-index', '150');

				d3.select(this).style('width', '160px');
				d3.select(this).style('height', function(){
					return event.currentTarget.clientHeight < 150 ? '60px' : false;
				});

				d3.select(this).select('span').classed('hidden', false);

			})
			.on('mouseout', function(d,i){

				var pr = filterProcess(d, selec),
					t = pr ? rScale(pr.quant) : 10;

				d3.select(this).select('span').classed('hidden', true);

				d3.select(this).style('opacity', '.8');

				d3.select(this).style('z-index', '10');

				d3.select(this).style('width', t + 'px');
				d3.select(this).style('height', t + 'px');

			});

	}

	/*

		Função do Update Processo e Ano

	 */

	function __updateBoxes(ano, selec){

		rScale.domain(__findDomain(selec));

		var box_upd = boxes.data(getYearDataset(ano));

		box_upd
			.style('width', function(d,i){
				var pr = filterProcess(d, selec);
				return (pr ? rScale(pr.quant) : 10) + 'px';
			})
			.style('height', function(d,i){
				var pr = filterProcess(d, selec);
				return (pr ? rScale(pr.quant) : 10) + 'px';
			})
			.style('display', function(d,i){
				var pr = filterProcess(d, selec);
				return pr ? 'block' : 'none';
			})
			.style('background-color', function(d,i){
				var pr = filterProcess(d, selec);
				return pr ? pr.cor : '#000';
			})
			.html(function(d,i){
				var pr = filterProcess(d, selec),
				q = pr ? pr.quant : '';
				return `<span class="hidden">${d.key}: ${q} processos</span>`;
			})
			.on('mouseover', function(d,i){

				d3.select(this).style('opacity', 1);

				d3.select(this).style('z-index', '150');

				d3.select(this).style('width', '160px');
				d3.select(this).style('height', function(){
					return event.currentTarget.clientHeight < 150 ? '60px' : false;
				});

				d3.select(this).select('span').classed('hidden', false);

			})
			.on('mouseout', function(d,i){

				var pr = filterProcess(d, selec),
					t = pr ? rScale(pr.quant) : 10;

				d3.select(this).select('span').classed('hidden', true);

				d3.select(this).style('opacity', '.8');

				d3.select(this).style('z-index', '10');

				d3.select(this).style('width', t + 'px');
				d3.select(this).style('height', t + 'px');

			});
		
	}

}