/*
	
	Mapa Agregado Ministério Público

 */

// queue

d3.queue()
	.defer(d3.json, './data/brasil_ufs.json') // mapa brasil topoJSON
	.defer(d3.json, './data/processos_agregado_uf.json') // Ministério Público Agregado 2012-2016
	.await(montaMapaAgregado);

function montaMapaAgregado(error, uf, mp) {

	// obter centro da UF

	var __getUFCenter = function(coords, uf){

		var resArray = coords[0].length == 1 ? coords[0][0] : coords[0];

		var xLon = d3.extent(resArray, function(val,i){
			return val[0];
		});

		var xLat = d3.extent(resArray, function(val,i){
			return val[1];
		});

		var centroX = xLon[0] + ( (xLon[1] - xLon[0]) / 2 );
		var centroY = xLat[0] + ( (xLat[1] - xLat[0]) / 2 );

		return {
			uf: uf || '',
			coords: [centroX, centroY]
		};

	};

	// cores rosa e creme para os circles

	var cores = {
		creme: '#fff8d8',
		rosa: '#EC7B7D'
	};

	// stage

	var w = 1000,
		h = 800;

	// transition

	var t = d3.transition().duration(750);

	// elemento svg

	var mpUf = d3.select('#mproc')
		.append('svg')
		.attr('width', w)
		.attr('height', h);

	// tooltip

	var tooltip = d3.select('body').append('div')
	 	.attr('class', 'hidden tooltip-acumulado');

	/*
		
		Projection

	 */

	var mercatorProjection = d3.geoMercator()
		.scale( 1000 )
		.translate( [w/2.2, h/2.2] );

	/**
	 * Obter latlong das UFs do Brasil a partir do topojson
	 *
	 * @return {array} Array de objetos
	 * @see https://github.com/topojson/topojson
	 */

	var BRlatLong = topojson.feature(uf, uf.objects.uf).features; // lat long das UFs do Brasil a partir do topojson

	/**
	 * Obter array com centróides das UFs (coordenadas)
	 *
	 * @return {array} Array de arrays com centróides de cada UF e string com nome das mesmas
	 */

	var centrosUF = BRlatLong.map(function(val, i){
		return __getUFCenter(val.geometry.coordinates, val.id);
	});

	// centralizar mapa

	mercatorProjection.center( __getUFCenter(BRlatLong[12].geometry.coordinates).coords ); // centralizar mapa (coords MT)

	if (error) throw error;

	/*
		
		Debulhar dados

	 */

	var processosUF = mp.map(function(val){
		return {'p': +val.quant, 'uf': val.UF};
	});

	/**
	 * Pegar corrdenadas a partir de string de UF
	 *
	 * @param {string} [uf] sigla uf
	 * @return {array} array com lat e long
	 */

	function latLong(uf){

		var c = centrosUF.filter(function(el,i){
			return el.uf == uf;
		});

		return c[0].coords;
	
	}

	/*
	
		Escalas

	 */

	var color = d3.scaleLog()
		.base(.5)
		.range(["#2F406A","#1F2C54"]) // + claro para o + escuro
		.domain(d3.extent(processosUF, function(el){
			return el.p;
		}));

	var makeIconProcesso = d3.scaleLinear()
		.range([20,50])
		.domain(d3.extent(processosUF, function(el){
			return el.p;
		}));


	/*
	
		Plot

	 */
	
	var path = d3.geoPath().projection(mercatorProjection); // cria as linhas do mapa

	// mapa

	mpUf.append('g')
		.attr('id','mapa_agregado')
		.selectAll('path')
		.data(BRlatLong)
		.enter()
		.append('path')
		.attr('d', path)
		.attr('fill', function(d,i){
			return color(processosUF[i].p);
		})
		.on('mouseover', function(el,i){
			d3.selectAll('.info').classed('hidden', true);
			d3.selectAll('.icon circle').attr('fill', cores.rosa);
		});

	// circulos rosa e ícone processos

	var selection = mpUf.append('g')
		.attr('id','icons')
		.selectAll('.icon')
		.data(processosUF)
		.enter()
		.append('g')
		.attr('class', function(el,i){
			return `icon ${el.uf}`;
		});

	selection.append('circle')
		.attr('fill', cores.rosa)
		.attr('cx', function(d,i){
			var ll = latLong(d.uf);
			return mercatorProjection(ll)[0];
		})
		.attr('cy', function(d,i){
			var ll = latLong(d.uf);
			return mercatorProjection(ll)[1];
		})
		.attr('r', function(d,i){
			return makeIconProcesso(d.p);
		});

	selection.append('image')
		.attr('xlink:href','img/icone_processos.svg')
		.attr('width', function(d,i){
			return makeIconProcesso(d.p);
		})
		.attr('height', function(d,i){
			return makeIconProcesso(d.p);
		})
		.attr('x', function(d,i){
			var ll = latLong(d.uf);
			return mercatorProjection(ll)[0];
		})
		.attr('y', function(d,i){
			var ll = latLong(d.uf);
			return mercatorProjection(ll)[1];
		})
		.attr('transform', function(d,i){
			var s = makeIconProcesso(d.p),
				d = (s/2) * -1;
			return `translate(${d},${d})`;
		})
		.on('mouseover', function(d, i){
			
			d3.selectAll('.icon').select('circle')
				.transition(t)
				.attr('r', function(d,i){
					return makeIconProcesso(d.p);
				});
			
			d3.select(this.previousSibling)
				.transition(t)
				.attr('r', function(d,i){
					return makeIconProcesso(d.p) + 10;
				})
				.attr('fill', cores.creme);
			
			d3.select(`.info.${d.uf}`).classed('hidden', false);

		})
		.on('mouseout', function(d){
			
			d3.select(this.previousSibling)
				.transition(t)
				.attr('r', function(d,i){
					return makeIconProcesso(d.p);
				});

		});

	// tabela legendas

	var colunas = ['UF','Processos'];

	var table = d3.select('#mapa_processos_acumulado').append('table').attr('id','info_table'),
		thead = table.append('thead');

	table.on('mouseout', function(el,i) {
		d3.selectAll('.info').classed('hidden', true);
	})

	thead.append('tr').selectAll('th')
		.data(colunas)
		.enter()
		.append('th')
		.text(function(el,i){
			return el;
		})

	var rows = table.selectAll('tr')
		.data(processosUF)
		.enter()
		.append('tr')
		.attr('data-uf', function(el,i){
			return el.uf;
		})
		.on('mouseover', function(el,i){
			
			d3.selectAll('.icon').select('circle')
				.transition(t)
				.attr('r', function(d,i){
					return makeIconProcesso(d.p);
				})
				.attr('fill', cores.rosa);
			
			d3.select('.icon.'+el.uf).select('circle')
				.transition(t)
				.attr('r', function(d,i){
					return makeIconProcesso(d.p) + 10;
				})
				.attr('fill', cores.creme);
			
			d3.select(`.info.${el.uf}`).classed('hidden', false);

		});

	rows.selectAll('td')
		.data(function(data){
			return colunas.map(function(el,i){
				var chave = el == 'UF' ? 'uf' : 'p';
				return {'coluna': el, 'valor': data[chave]};
			})
		})
		.enter()
		.append('td')
		.text(function(el,i){
			return el.valor;
		});

	// circulos com texto

	var infoBolas = mpUf.append('g')
		.attr('id','circles-values')
		.selectAll('g')
		.data(processosUF)
		.enter()
		.append('g')
		.attr('class', function(d,i){
			return `info ${d.uf} hidden`;
		})
		.attr('transform', function(el,i){
			var r = makeIconProcesso(el.p) + 6;
			return `translate(${r}, ${r * -1})`; // x y
		});

	infoBolas.append('circle')
		.attr('r', '25')
		.attr('cx', function(d,i){
			var ll = latLong(d.uf);
			return mercatorProjection(ll)[0];
		})
		.attr('cy', function(d,i){
			var ll = latLong(d.uf);
			return mercatorProjection(ll)[1];
		})
		.attr('fill', '#FFF8D8');

	infoBolas.append('text')
		.attr('x', function(d,i){
			var ll = latLong(d.uf);
			return mercatorProjection(ll)[0];
		})
		.attr('y', function(d,i){
			var ll = latLong(d.uf);
			return mercatorProjection(ll)[1];
		})
		.attr('dx', -14)
		.attr('dy', 5)
		.text(function(d,i){
			return d.p;
		});

}