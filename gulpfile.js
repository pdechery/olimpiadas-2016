var gulp = require('gulp'),
	less = require('gulp-less'),
	coffee = require('gulp-coffee'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	//sourcemap = require('gulp-sourcemaps'),
	browserify = require('gulp-browserify');

gulp.task('less',function(){

	return gulp.src('assets/less/*.less')
		.pipe(less())
		.pipe(gulp.dest('css'));
		
});

gulp.task('browserify', function(){

	return gulp.src('assets/coffee/main.coffee', { read: false })
		.pipe(browserify({
			transform: ['coffeeify'],
			extensions: ['.coffee'],
			debug: true
			})
		)
		.pipe(concat('bundle.js'))
		//.pipe(uglify()) somente producao, aplicar sourcemaps qdo possivel.
		.pipe(gulp.dest('js'))
		
})

gulp.task('watch', function(){

	gulp.watch('assets/less/*.less',['less']);
	gulp.watch('assets/coffee/main.coffee',['browserify']);

});

gulp.task('default', ['watch']);