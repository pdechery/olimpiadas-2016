#
# Nuvem Palavras
# ---------------------------
# Pierre Dechery | Julho 2016
#

color = d3.scale.linear()
	.domain([0,1,2,3,4,5,6,10,15,20,100])
	.range(["#106835", "#1e9146", "#3eb049", "#67bd45", "#99ca3c", "#afd136"])

_file_ = "data/tag-cloud.json"

d3.json(_file_, (x, data)->

	d3cloud()
		.size([800,200])
		.words(data)
		.rotate(0)
		.fontSize((d)-> 
			d.size
		)
		.on('end', draw)
		.start()

)

draw = (words)->

	d3.select('#cloud').append('svg')
		.attr('width', 850)
		.attr('height', 250)
		.attr('class', 'cloud-canvas')
		.append('g')
		.attr('transform','translate(380,140)')
		.selectAll('text')
		.data(words)
		.enter()
		.append('text')
		.style('font-size', (d)->
			"#{d.size}px"
		)
		.style('fill', (d,i)->
			color(i)
		)
		.attr('transform', (d)->
			"translate(#{d.x},#{d.y}) rotate(#{d.rotate})"
		)
		.text((d)-> d.text)