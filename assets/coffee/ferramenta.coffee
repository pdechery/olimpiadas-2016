
# funcao principal

ChartSentimento = ()->

	margin = {top: 10, right: 20, bottom: 40, left: 20}
	width = 1000 - margin.left - margin.right
	height = 400 - margin.top - margin.bottom

	parseDate = d3.time.format("%Y-%m-%d").parse

	dayNameFormat = d3.time.format("%d/%m")

	dayFormat = d3.time.format("%d")

	x = d3.time.scale().range([0, width])

	y = d3.scale.linear().range([height, 0])

	z = d3.scale.ordinal().range(["#F8D763", "#57BD6E", "#56A7DC", "#E9000D"])

	# função que calcula o shape

	area = d3.svg.area()
		.interpolate("basis")
		.x((d)-> x(d.date))
		.y0((d)-> y(d.neg))
		.y1((d)-> y(d.pos))

	# stage

	svg = d3.select("#chart #svg_container").append("svg")
		# .attr("width", width + margin.left + margin.right)
		# .attr("height", height + margin.top + margin.bottom)
		.attr("viewBox","0 0 #{width + margin.left + margin.right} #{height + margin.top + margin.bottom}")
		.append("g")
		.attr("transform", "translate(#{margin.left},#{margin.top})")

	# d3 tip

	tooltip = d3.tip()
		.attr('class','d3-tip-exp pos')
		.direction('n')
		.offset([-20, 0])
		.html((d) -> 
			html = "<p class='data'>#{dayNameFormat(d.date)}</p>"
			html += "<p class='mencoes'>MENÇÕES: <strong>#{d.pos}</strong><br>"
			html += "<span>/mentions</span></p>"
			html
		)

	tooltip2 = d3.tip()
		.attr('class','d3-tip-exp neg')
		.direction('n')
		.offset([-20, 0])
		.html((d) -> 
			html = "<p class='data'>#{dayNameFormat(d.date)}</p>"
			html += "<p class='mencoes'>MENÇÕES: <strong>#{Math.abs(d.neg)}</strong><br>"
			html += "<span>/mentions</span></p>"
			html
		)

	# dados

	data = []

	# valor minimo para eixo vertical

	min_y = 0

	max_y = 0

	min_x = 0

	max_x = 0

	# função que recebe os dados

	chart = (i, d)->

		data = setupData(d)

		# domains dinamicos

		min_x = d3.min(data, (d)->
			d3.min(d.dias, (d)-> d.date)
		)

		max_x = d3.max(data, (d)->
			d3.max(d.dias, (d)-> d.date)
		)

		min_y = d3.min(data, (d)->
			d3.min(d.dias, (d)-> d.neg)
		)

		max_y = d3.max(data, (d)->
			d3.max(d.dias, (d)-> d.pos)
		)

		x.domain([min_x, max_x])

		y.domain([min_y, max_y])

		# eixos

		render()

		return true

		# update

	render = ()->

		# areas

		svg.data(data)

		# linhas

		gLinhas = svg.append('svg:g').attr('class','linhas').data(data)

		gLinhas.selectAll('line')
			.data((d)-> d.dias)
			.enter()
			.append('svg:line')
			.attr('x1', (d)-> x(d.date))
			.attr('x2', (d)-> x(d.date))
			.attr('y1', (d)-> y(min_y))
			.attr('y2', (d)-> y(max_y))

		# paths

		svg.selectAll('.area')
			.data(data)
			.enter()
			.append('path')
			.attr("fill", (d,i)-> z(i))
			.attr("class", (d)->
				"area #{d.categoria}"
			)
			.attr("d", (d)->
				area(d.dias)
			)

		# dots

		dotGroup = svg.selectAll('g.group')
			.data(data)
			.enter()
			.append('svg:g')
			.attr('class', (d)->
				"group #{d.categoria}"
			)

		dotGroup.selectAll('pos-dot')
			.data((d)-> d.dias)
			.enter()
			.append('circle')
			.attr('class', 'pos-dot')
			.attr('r', 5)
			.call(tooltip)
			.on('mouseover', tooltip.show)
			.on('mouseout', tooltip.hide)
			.attr('cx',(d)-> x(d.date))
			.attr('cy',(d)-> y(d.pos))

		dotGroup.selectAll('neg-dot')
			.data((d)-> d.dias)
			.enter()
			.append('circle')
			.attr('class','neg-dot')
			.attr('r', 5)
			.call(tooltip2)
			.on('mouseover', tooltip2.show)
			.on('mouseout', tooltip2.hide)
			.attr('cx',(d)-> x(d.date))
			.attr('cy',(d)-> y(d.neg))

		# -> eixo horizontal

		svg.append('line')
			.attr('class', 'eixo_h')
			.attr('x1', (d)-> x(0))
			.attr('x2', 1200)
			.attr('y1', (d)-> y(0))
			.attr('y2', (d)-> y(0))

		# numeros dias

		gDias = svg.append('svg:g')
			.attr('id','dias')
			.attr("transform", "translate(-10,0)")
			.data(data)

		gDias.selectAll('text')
			.data((d)-> d.dias)
			.enter()
			.append('svg:text')
			.attr('x', (d)-> x(d.date))
			.attr('y', 390)
			.attr('class','text')
			.text((d)-> dayFormat(d.date))

		return true

	# tratamento dados

	setupData = (data)->

		data.forEach((d)->

			d.dias.forEach((d)->

				d.date = parseDate(d.date)
				d.pos = +d.pos
				d.neg = +d.neg * -1

				return true

			)

		)

		return data

	return chart

# menu switch paths

d3.selectAll('nav#switch ul li')
	.on('click',()->
		
		target = d3.select(this).attr('data-categ')

		if target != 'all'

			d3.selectAll('.area, .group').classed('off', ()->

				!d3.select(this).classed(target)

			)

		else

			d3.selectAll('.area, .group').classed('off', false)

	)
	

module.exports = ChartSentimento