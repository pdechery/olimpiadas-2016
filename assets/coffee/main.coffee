#
# JS Site Olimpíadas
# ---------------------------
# Pierre Dechery | Julho 2016
#

jQuery = require('jquery')
require('../vendor/jquery.fullpage.min')
require('perfect-scrollbar/jquery')(jQuery)

global.d3 = require('../vendor/d3')

d3tip_module = require('d3-tip')
d3tip_module(d3)

global.d3cloud = require('d3-cloud')


#
# Script Principais
#

(($)->
	$(()->

		chartSent = require('./ferramenta')()

		chartTerm = require('./termometro')()

		#
		# Funções Principais
		#

		if typeof $.fn.fullpage.destroy == 'function'
			$.fn.fullpage.destroy('all')

		$('#container').fullpage({
			sectionSelector: '.panel'
			slideSelector: '.slide'
			css3: true
			loopHorizontal: false
			menu: 'false'
			anchors: ['ferramenta']
			controlArrows: false
			afterSlideLoad: (anchor,index,slide,slideIndex)->

				$('#arrow-nav').removeClass('off')

				$menuslide = $('nav.menu-slides a')

				$video = $('#video')

				if slide == 'experiencia'

					# animação menu

					$menuslide.removeClass('ativo').filter('[data-secao=exp]').addClass('ativo')

					# troca video

					$video.find('source').attr('src', 'media/vlt.mp4')

					$video.get(0).load()

				else if slide == 'experiencia-vis'

					# inicia ferramenta sentimento

					_file_ = "data/ferramenta.json"

					d3.json(_file_, chartSent)

				else if slide == 'termometro'

					# animação menu

					$menuslide.removeClass('ativo').filter('[data-secao=ter]').addClass('ativo')

					# troca video

					$video.find('source').attr('src', 'media/termometro_de_temas.mp4')

					$video.get(0).load()

				else if slide == 'termometro-vis'

					# animacao aqui

					_file_ = "data/termometro-temas.json"

					d3.json(_file_, chartTerm)

				else if slide == 'time-brasil'

					# animação menu

					$menuslide.removeClass('ativo').filter('[data-secao=tim]').addClass('ativo')

					# troca video

					$video.find('source').attr('src', 'media/museu_cima_baixo.mp4')

					$video.get(0).load()

				else if slide == 'time-brasil-vis'

					require('./cloud')		

				if slide == 'insights'

					$('#arrow-nav').addClass('off')

					# animação menu

					$menuslide.removeClass('ativo').filter('[data-secao=ins]').addClass('ativo')

					# troca video

					$video.find('source').attr('src', 'media/tabelas.mp4')

					$video.get(0).load()

		})

		# clique na seta

		$('#arrow-nav').addClass('animated shake').on('click', ()-> $.fn.fullpage.moveSlideRight() )

		# deixar marcado primeiro slide ativo

		$('nav.menu-slides li a').first().addClass('ativo')

		$('#vis-time .col').perfectScrollbar()

		# overlay

		$('.open_overlay').add('.close_overlay').on('click', ()->

			target = $(this).data('target')
			
			$(target).toggleClass('aberto')

		)

	)
)(jQuery)