
# funcao principal

ChartTermometro = ()->

	width = 1000
	height = 230

	radian = d3.scale.linear().range([0, 2 * Math.PI])

	# cores

	z = d3.scale.ordinal().range(["#1f77b4", "#ff7f0e", "#2ca02c"])

	# raio

	r = 115

	# dados

	data = []

	# arcos

	arcs = ''

	# fazer os arcos

	arc = d3.svg.arc()
		.innerRadius(r - 30)
		.outerRadius(r)
		.startAngle(0)
		.endAngle((d)-> radian(d))

	# stage

	svg = d3.select("#vis-container").append("svg")
		.attr("width", width)
		.attr("height", height)

	# tooltip

	tooltip = d3.tip()
		.attr('class','d3-tip-term')
		.direction('s')
		.offset([-20, 0])
		.html((d) -> 
			html = "<p>Volume de menções</p>"
			html += "<p>#{d}</p>"
			html
		)

	# função que recebe os dados

	chart = (i, d)->

		data = setupData(d)

		max_pt = d3.max(data, (d)-> d.tweets.pt)

		max_en = d3.max(data, (d)-> d.tweets.en)

		max = d3.max([max_pt,max_en])

		#radian.domain([0, max])
		radian.domain([0,600]) # -> maximo arbitrário, como calcular dinamico ?

		render()

		return true

	render = ()->

		groups = svg.selectAll('g')
			.data(data)
			.enter()
			.append('svg:g')
			.attr("class", (d)-> "tema #{d.categoria}")
			.attr("fill", (d,i)->
				z(i)
			)
			.attr("transform",(d,i)->
				# so para 2 categorias
				if i == 0
					"translate(0,0)"
				else
					"translate(520,0)"
			)

		groups.append('image')
			.attr('xlink:href','img/fundo_velocimetro4.png')
			.attr('width', 480)
			.attr('height', 230)
			.attr('x', 0)
			.attr('y', 0)

		arcs = groups.selectAll('.arc')
			.data((d)-> [d.tweets.pt,d.tweets.en])
			.enter()
			.append('path')
			.attr('class','arc')
			.attr("transform",(d,i)->
				# so para 2 categorias
				if i == 0
					"translate(115,115)"
				else
					"translate(365,115)"
			)
			.call(tooltip)
			.on('mouseover', tooltip.show)
			.on('mouseout', tooltip.hide)
		
		arcs.transition().duration(750).attrTween("d", arcTween)

		return true

	arcTween = (a)->
				
		i = d3.interpolate(0,a)

		(t)->

			arc(i(t))

	# tratamento dados

	setupData = (data)->

		data.forEach((d)->

			d.tweets.pt = +d.tweets.pt
			d.tweets.en = +d.tweets.en

			return true

		)

		return data

	return chart
	

module.exports = ChartTermometro