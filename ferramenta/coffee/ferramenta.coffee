
# funcao principal

ChartOlimpiadas = ()->

	margin = {top: 70, right: 20, bottom: 30, left: 50}
	width = 960 - margin.left - margin.right
	height = 500 - margin.top - margin.bottom

	parseDate = d3.time.format("%d-%b-%y").parse

	x = d3.time.scale().range([0, width])

	y = d3.scale.linear().range([height, 0])

	z = d3.scale.ordinal().range(["#1f77b4", "#ff7f0e", "#2ca02c"])

	xAxis = d3.svg.axis()
		.scale(x)
		.ticks(4)
		.orient("bottom")

	yAxis = d3.svg.axis()
		.scale(y)
		.orient("left")

	# função que calcula o shape

	area = d3.svg.area()
		.interpolate("basis")
		.x((d)-> x(d.date))
		.y0((d)-> y(d.neg))
		.y1((d)-> y(d.pos))

	# stage

	svg = d3.select("#chart").append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
		.attr("transform", "translate(#{margin.left},#{margin.top})")

	# d3 tip

	tooltip = d3.tip()
		.attr('class','d3-tip')
		.html((d) -> "#{d.pos}")

	tooltip2 = d3.tip()
		.attr('class','d3-tip')
		.html((d) -> "#{d.neg}")

	data = []

	# função que recebe os dados

	chart = (i, d)->

		data = setupData(d)

		# domains dinamicos

		min_x = d3.min(data, (d)->
			d3.min(d.dias, (d)-> d.date)
		)

		max_x = d3.max(data, (d)->
			d3.max(d.dias, (d)-> d.date)
		)

		min_y = d3.min(data, (d)->
			d3.min(d.dias, (d)-> d.neg)
		)

		max_y = d3.max(data, (d)->
			d3.max(d.dias, (d)-> d.pos)
		)

		x.domain([min_x, max_x])

		y.domain([min_y, max_y])

		# eixos

		svg.append("g")
			.attr("class", "x axis")
			.attr("transform", "translate(0," + height + ")")
			.style("font-size", ".6em")
			.call(xAxis)

		svg.append("g")
			.attr("class", "y axis")
			.call(yAxis)
			.style("font-size", ".6em")
			.append("text")
			.attr("transform", "rotate(-90)")
			.attr("y", 6)
			.attr("dy", ".71em")
			.style("text-anchor", "end")

		render()

		return true

		# update

	render = ()->

		# areas

		svg.data(data)

		chart = d3.select('#chart')

		# paths

		svg.selectAll('.area')
			.data(data)
			.enter()
			.append('path')
			.attr("fill", (d,i)-> z(i))
			.attr("class", (d)->
				"area #{d.categoria}"
			)
			.attr("d", (d)->
				area(d.dias)
			)

		# dots

		dotGroup = svg.selectAll('g.group')
			.data(data)
			.enter()
			.append('svg:g')
			.attr('class', (d)->
				"group #{d.categoria}"
			)

		dotGroup.selectAll('pos-dot')
			.data((d)-> d.dias)
			.enter()
			.append('circle')
			.attr('class', 'pos-dot')
			.attr('r', 5)
			.call(tooltip)
			.on('mouseover',tooltip.show)
			.on('mouseout',tooltip.hide)
			.attr('cx',(d)-> x(d.date))
			.attr('cy',(d)-> y(d.pos))

		dotGroup.selectAll('neg-dot')
			.data((d)-> d.dias)
			.enter()
			.append('circle')
			.attr('class','neg-dot')
			.attr('r', 5)
			.call(tooltip2)
			.on('mouseover',tooltip2.show)
			.on('mouseout',tooltip2.hide)
			.attr('cx',(d)-> x(d.date))
			.attr('cy',(d)-> y(d.neg))

		return true

	# tratamento dados

	setupData = (data)->

		data.forEach((d)->

			d.dias.forEach((d)->

				d.date = parseDate(d.date)
				d.pos = +d.pos
				d.neg = +d.neg * -1

				return true

			)

		)

		return data

	return chart

# menu switch paths

d3.selectAll('#chart + ul li')
	.on('click',()->
		
		target = d3.select(this).attr('data-categ')

		d3.selectAll('.group').classed('off', ()->
			!d3.select(this).classed(target)
		)

		d3.selectAll('.area').classed('off', ()-> 
			!d3.select(this).classed(target)
		)
	)
	

# instanciar a função

newChart = ChartOlimpiadas()

# request arquivo externo

_file_ = "data/olimpiadas.json"

d3.json(_file_, newChart)