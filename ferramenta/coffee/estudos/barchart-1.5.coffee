#
# Definindo função
#

negativeChart = ()->

	# dimensoes do gráfico

	width = 600
	height = 340

	bars = null

	# escalas (vertical e horizontal)

	x = d3.scale.ordinal().rangeRoundBands([0, width], 0.1)

	y = d3.scale.linear().range([height, 0])

	# eixos (axis)

	xAxis = d3.svg.axis()
		.orient("bottom")
		.scale(x)

	yAxis = d3.svg.axis()
		.orient("left")
		.tickPadding('14')
		.innerTickSize(0)
		.outerTickSize(0)
		.scale(y)

	# tip

	tip = d3.tip().attr('class','d3-tip')
		.html((d)-> "<p>#{d.value}</p>")

	nChart = (el, data)->

		# cria o svg

		svg = d3.select('#barchart .wrap')
			.append('svg')
			.attr('id','chart')
			.attr('width', width+40)
			.attr('height', height+20)
			.append('g')
			.attr('transform','translate(40,10)')

		# dominios x e y: limites dos dados

		x.domain(data.map((d)-> d.name ))

		y.domain(d3.extent(data,(d)-> d.value)).nice()

		# apendar rects no svg

		bars = svg.selectAll('.bar')
			.data(data)
			.enter()
			.append('rect')
			.attr('class', (d)-> if d.value > 0 then 'bar pos' else 'bar neg')
			.attr('x',(d)-> x(d.name))
			.attr('y',(d)->
				
				# se for negativo retorna 0
				# se for positivo retorna d.value

				y(Math.max(0, d.value))

			)
			.attr('height',0)
			.attr('width', x.rangeBand())
			.call(tip)
			.on('mouseover', tip.show)
			.on('mouseout', tip.hide)

		# eixo vertical

		svg.append('g')
			.attr('transform','translate(0,0)')
			.attr('class','axis')
			.call(yAxis)

	nChart.render = ()->
		bars.transition().delay(500).duration(1000).attr('height', (d)-> Math.abs(y(d.value) - y(0)))

	nChart

#
# Invocando a função
#

# sem isso javascript interpreta 3 < 9

type = (d)->
	d.value = +d.value
	d

_file_ = 'data/olimpiadasII.csv'

myNChart = negativeChart()

d3.csv(_file_, type, (data)->
	myNChart('#barchart .wrap', data)
)