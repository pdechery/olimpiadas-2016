// Generated by CoffeeScript 1.10.0
(function() {
  var height, svg, tip, type, width, x, xAxis, y, yAxis;

  width = 600;

  height = 300;

  x = d3.scale.ordinal().rangeRoundBands([0, width], 0.1);

  y = d3.scale.linear().range([height, 0]);

  xAxis = d3.svg.axis().orient("bottom").scale(x);

  yAxis = d3.svg.axis().orient("left").tickPadding('14').innerTickSize(0).outerTickSize(0).scale(y);

  svg = d3.select('#barchart .wrap').append('svg').attr('width', width + 300).attr('height', height + 50).append('g').attr('transform', 'translate(200,40)');

  tip = d3.tip().attr('class', 'd3-tip').html(function(d) {
    return "<p>" + d.value + "</p>";
  });

  type = function(d) {
    d.value = +d.value;
    return d;
  };

  d3.csv('data/olimpiadasII.csv', type, function(data) {
    var bars;
    x.domain(data.map(function(d) {
      return d.name;
    }));
    y.domain(d3.extent(data, function(d) {
      return d.value;
    })).nice();
    bars = svg.selectAll('.bar').data(data).enter().append('rect').attr('class', function(d) {
      if (d.value > 0) {
        return 'bar pos';
      } else {
        return 'bar neg';
      }
    }).attr('x', function(d) {
      return x(d.name);
    }).attr('y', function(d) {
      return y(Math.max(0, d.value));
    }).attr('width', x.rangeBand()).attr('height', function(d) {
      return Math.abs(y(d.value) - y(0));
    }).call(tip).on('mouseover', tip.show).on('mouseout', tip.hide);
    return svg.append('g').attr('transform', 'translate(0,0)').attr('class', 'axis').call(yAxis);
  });

}).call(this);
