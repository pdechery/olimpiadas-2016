// dimensoes do gráfico

var width = 900,
	height = 700;

// escalas (vertical e horizontal)

var x = d3.scale.ordinal()
	.rangeRoundBands([0, width], 0.1);

var y = d3.scale.linear().range([height, 0]);

// eixos (axis)

var xAxis = d3.svg.axis()
	.orient("bottom")
	.scale(x);

var yAxis = d3.svg.axis()
	.orient("right")
	.scale(y);

// cria o svg

var svg = d3.select('body')
	.append('svg')
	.attr('width', width)
	.attr('height', height + 120)
	.append('g')
	.attr('transform','translate(0, 90)');

// tip

var tip = d3.tip().attr('class','d3-tip')
	.html(function(d){
		return "<p>"+d.value+"</p>";
	})

// d3.csv, passar dados do csv externo

d3.csv('data/olimpiadasII.csv', type, function(data){

	// dominios x e y: limites dos dados

	x.domain(data.map(function(d){
		return d.name;
	}));

	y.domain(d3.extent(data, function(d){
		return d.value;
	})).nice();

	// apendar rects no svg

	var bars = svg.selectAll('.bar')
		.data(data)
		.enter()
		.append('rect')
		.attr('class', function(d){

			return d.value > 0 ? 'bar pos' : 'bar neg';

		})
		.attr('x',function(d){
			
			return x(d.name);

		})
		.attr('y',function(d){
			
			// se for negativo retorna 0
			// se for positivo retorna d.value

			return y(Math.max(0, d.value));

		})
		.attr('width', x.rangeBand())
		.attr('height', function(d){
			
			// y(0) corresponde ao 0 do eixo vertical

			return Math.abs(y(d.value) - y(0));

		})
		.call(tip)
		.on('mouseover', tip.show)
		.on('mouseout', tip.hide);

	// render dos axis, elements 'g'

	svg.append('g')
		.attr('transform','translate(0,'+ y(0) +')')
		.call(xAxis);

	svg.append('g')
		.attr('transform','translate(0,0)')
		.call(yAxis);

});

// sem isso javascript interpreta 3 < 9

function type(d) {
	d.value = +d.value;
	return d;
}